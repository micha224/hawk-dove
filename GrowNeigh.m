function [NE] = GrowNeigh(NE, growth)
% GROWNEIGH Identifies dead cells and allows them to grow, assuming the same
% value as the most occurring calue in 4-neighbourhood in array NE 
% Runa Magnusson, Feb 2017 
% For practical Evolution & Behaviour (Dr. M. Boerlijst)

% get dimensions of input
N = size(NE, 1);

% create random field 0 - 1
A = rand(N,N);

% evaluate which cells are dead now and allow a fraction of these to grow
[r,c] = find(((A < growth) .* (NE == 0)) > 0);

% change these cells
for l = 1:length(r);
    l
    length(r)
    'check'
    row = r(l);
    col = c(l);
    
    % get values of neighbours (4-neighbourhood)
    neighs = [NE(row, cdc(col-1,N)), NE(row, cdc(col+1,N)), ...
        NE(cdc((row-1),N), col), NE(cdc((row+1),N), col)];
    
    % determine most occurring value or pick random in case of a tie
    [m,f,cc] = mode(neighs);
    if size(cell2mat(cc),1) < 2
        val = m
    else
        val = neighs(randi(4))
    end

    NE(row,col) = val;
end % for .. 

end % function
