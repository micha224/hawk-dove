%% ------------------------------------------------------------
%  SPATIAL ALTRUISM MODEL

% Adapted by Maarten Boerlijst & Runa Magnusson
%  <boerlijst@uva.nl>

% The model simulates spread of an altruistic gene that decreases the
% probability of dying of neighbour individuals, but increases the
% deathrate of the altruistic individuals

%% 1.1 INITIALISATION - MODEL ESSENTIALS
% Do not change

clc; clear all; close all;

% build colormap
map = [ 0 0 0 
        0 1 0 
        1 0 0 
        1 1 0];

%% 1.2 INITIALISATION - CONTROL CONSTANTS
% Set size, time span, delay and input/output of the model
    
% delay factor in seconds (delay=0 means no delay)  
delay = 0.01;  
    
% set interval between drawing states (speeds up run time)
drawstep=10;

% set name of matrix to load or save
startfile = 'MutStart.mat';

% Set Parameters
max_iter = 500;

% Automaton size
N = 100;

%% 1.3 INITIALISATION - SYSTEM CONSTANTS
% Set system parameters that will control species distribution and
% behaviour

% basic growth rate
p_growth = 0.1;

% deathrate probability  
p_death = 0.06;

% costs -> growth rate reduction (cannot exceed p_growth)
_cost = 0.08;
mut_cost = 0.08;

% Benefit of altruism -> growth rate increase per altruist neighbour
alt_benefit = 0.05;
mut_benefit = 0.05;

% Movement probability
p_move = 0.0;

%% 1.4 INITIALISATION - ALLOCATIONS
% Do not change

% Allocations of system variables
pA = zeros(max_iter+1,1);
pM = zeros(max_iter+1,1);
pU = zeros(max_iter+1,1);
pE = zeros(max_iter+1,1);
clusterAA = zeros(max_iter+1,1);
clusterEE = zeros(max_iter+1,1);
clusterAE = zeros(max_iter+1,1);
E = zeros(N,N);
NE = zeros(N,N);
Pgrowth= zeros(N,N);

% determine maximum growth rate  
maxgrowth =max(p_growth+8*alt_benefit,p_growth+8*mut_benefit) ;


%% 2.1 MODEL - INITIAL CONDITIONS

% load an existing or a custom start situation

A = rand(N,N);
I = find(A < 0.04);
E(I) = 1;
I = find(A < 0.01);
E(I) = 2;

startmap = csvread('middle start.csv');
E = startmap;

% introduce the mutant in the right half of the field
%{
for i=1:N
    for j=N/2:N
       if (E(i,j) == 1)
          E(i,j)=3;
       end
    end
end
%}
%draw initial state and pause
figure(1);
set(gcf,'Position',[230 130 400 380])
image(E+1);  
colormap(map);
title('Initial Pattern -> hit key to continue')
pause;

ButtonH = uicontrol('Style', 'ToggleButton', 'Position',[5 5 60 30], ...
    'String', 'HALT', 'Value', 0);

f2=figure(2); clf
set(gcf,'Position',[700 130 450 380])





%% 2.2 MODEL - GLOBAL LOOP
for iter=0:max_iter
    
    % default newstate = oldstate
    NE=E;
    
    % counting the number of altruists in the 8 cell neighborhood
    Ncount1=CountNeigh(1,E);
    Ncount3=CountNeigh(3,E);
    
    % calculate growthrates  
    for i=1:N
        for j=1:N
            switch E(i,j)  
                case 0 
                    Pgrowth(i,j)=0;
                case 1
                    Pgrowth(i,j)=p_growth-alt_cost+alt_benefit*Ncount1(i,j)+mut_benefit*Ncount3(i,j);
                case 2
                    Pgrowth(i,j)=p_growth+alt_benefit*Ncount1(i,j)+mut_benefit*Ncount3(i,j);
                case 3
                    Pgrowth(i,j)=p_growth-mut_cost+alt_benefit*Ncount1(i,j)+mut_benefit*Ncount3(i,j);
           end
        end
    end                
                    
    % Calculating Next State
    for i=1:N
        for j=1:N
            switch E(i,j)  
                case 0     % if cell is empty -> random growth with probability p_growth
                   neigh=randi(8); % pick a random neighbour
                    if (rand < SelectNeigh(Pgrowth,neigh,i,j))   % check the growth rate
                        NE(i,j)= SelectNeigh(E,neigh,i,j);  %create an offspring
                     end
                otherwise   
                     if (rand < p_death)   % density-independent deathrate
                         NE(i,j)=0;
                     end
            end
         end
    end

    
    
    
    %% 2.3 MODEL - VISUALISATION
    
    if mod(iter,drawstep)==0
    
     % drawing state 
       figure(1)
       imagesc(E); 
       colormap(map)
       caxis([0 3])
       title(['Iteration --> ',num2str(iter)])
       drawnow
    

    % drawing payments 

        figure(2)
        imagesc(Pgrowth);
        colormap('Bone');
        colorbar;
        caxis([0 maxgrowth])
        title('growthrate')
        drawnow
    
   
  
    % if delay was set to fixed amount of seconds, wait that amount
  
        pause(delay)
    end
    
    %% 2.4 MODEL- EVALUATE USER CONTROL PANEL
    % If user has pressed stop button, break to implement a change in the 
    % system 

   if get(ButtonH, 'Value') == 1
    title(['Iteration --> ',num2str(iter),'    hit key to continue'])
    pause;
    figure(1)
    ButtonH = uicontrol('Style', 'ToggleButton', 'Position',[5 5 60 30], ...
    'String', 'HALT', 'Value', 0);
   end
 
   
    %% 2.5 MODEL - STATS + UPDATE
    
    % proportion of cooperators, egoists and dead cells
    pA(iter+1) = length(find(E==1)) / (N*N);
    pM(iter+1) = length(find(E==3)) / (N*N);
    pE(iter+1) = length(find(E==2)) / (N*N);
    pU(iter+1) = length(find(E==0)) / (N*N);
    
    % clustering of coorperators and defectors
    % theoretical clustering based on proportions
    names = {'dead-dead';'dead-alt';'dead-ego';'ego-ego'; 'ego-alt'; 'alt-alt'};
    expe = [pU(iter+1)^2; 2*pU(iter+1)*pA(iter+1); 2*pU(iter+1)*pE(iter+1); ...
    pE(iter+1)^2; 2*pE(iter+1)*pA(iter+1); pA(iter+1)^2];

    % actual clustering (counted in clustercalc function)
    obs = clustercalc(NE,4);
    if (pA(iter+1)> 0) 
        clusterAA(iter+1) = obs(2,2)/expe(6);
    end
    if (pE(iter+1)>0)
        clusterEE(iter+1) = obs(3,3)/expe(4);
    end
    if (pA(iter+1) > 0 && pE(iter+1) > 0)
        clusterAE(iter+1) = (obs(2,3)+obs(3,2))/expe(5);
    end
    
    % updating the next state to the current state
    E = NE;

end %iter

%% 3.1 OUTPUT - FINISH .AVI MOVIES AND PLOT STATISTICS


% timeplot PROPORTION of cooperators
figure(4);
set(gcf,'Position',[160 660 440 340])
plot(0:max_iter,pA,'-go');
hold on;
plot(0:max_iter,pM,'-yo');
hold on;
plot(0:max_iter,pE,'-ro');
hold on;
plot(0:max_iter,pU,'-ko');
xlabel('Iteration')
ylabel('Proportion')
legend({'altruists', 'mutant', 'egoists', 'unoccupied'})
grid on;

% timeplot CLUSTERING of cooperators
% figure(5)
% set(gcf,'Position',[610 660 440 340])
% plot(0:max_iter,clusterAA,'-gs')
% hold on
% plot(0:max_iter,clusterEE,'-rs')
% xlabel('Iteration')
% ylabel('Clustering Index')
% legend({'alt-alt', 'ego-ego'})
% grid on;
% 
% figure(6)
% set(gcf,'Position',[800 660 440 340])
% plot(0:max_iter,clusterAE,'-bs')
% xlabel('Iteration')
% ylabel('Clustering Index')
% legend({'alt-ego'})
% grid on;

% if desired: save state created
save('EndState.mat','E')