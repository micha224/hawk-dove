function [y] = SelectNeigh(A,rn,i,j)
%   return the state of a random neighbour 
%   using eight cell neighborhood
%   includes torus boundaries 

[m,n] = size(A);

  switch rn
    case 1
        y=A(cdc(i-1,m),cdc(j+1,n));  %SW
    case 2
        y=A(i,cdc(j+1,n));           %S
    case 3
        y=A(cdc(i+1,m),cdc(j+1,n));  %SE
    case 4
        y=A(cdc(i-1,m),cdc(j-1,n));  %NW
    case 5
        y=A(i,cdc(j-1,n));           %N
    case 6
        y=A(cdc(i+1,m),cdc(j-1,n));  %NE
    case 7
        y=A(cdc(i-1,m),j);           %W
    case 8
        y=A(cdc(i+1,m),j);           %E
  end
end
             
