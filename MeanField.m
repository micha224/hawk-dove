function [y] = MeanField(A)
%   randomize the field in 2D matrix A 

[m,n] = size(A);

ix = randperm(m*n);

y = reshape(A(ix),m,n);
 
end
             
