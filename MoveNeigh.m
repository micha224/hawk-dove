function [ NE ] = MoveNeigh( NE, r, c )
% MOVENEIGH Moves a cell in array NE with a neighbour (randomly picked from
% its 4 neighbours)
% Runa Magnusson, Feb 2017 
% For practical Evolution & Behaviour (Dr. M. Boerlijst)
%
%   INPUTS
%   NE: array with cell states
%   r: rows of positions to be changed
%   c: columns of positions to be changed
%
%   OUTPUTS
%   NE: updated cell states

    % get dimensions of NE
    [N,cols] = size(NE);
   
    for i = 1:length(r)      
        % determine with which neighbour to switch
        nb = randi([1 4]);
        switch nb
        % 1 = upper neighbour, 2 = right neighbour, 3 = lower neighbour, 4
        % = left neighbour
        case 1
            k = cdc(r(i)-1,N);
            l = c(i);
        case 2
            k = r(i);
            l = cdc(c(i)+1,N);
        case 3
            k = cdc(r(i)+1,N);
            l = c(i);
        case 4
            k = r(i);
            l = cdc(c(i)-1,N);
        end
        % swap current cell and its neighbour
        old = NE(r(i),c(i));
        NE(r(i), c(i)) = NE(k,l);
        NE(k,l) = old;
    end

end % for i = ...

