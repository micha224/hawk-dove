function [score] = clustercalc(A,nspec)
%CLUSTERCALC Summary of this function goes here
%   Calculates pairwise clustering 
% Runa Magnusson & Maarten Boerlijst, February 2017
% uses 4-neighbourhood


% initialize a counter matrix to keep scores of occurrence of different
% neighbourhood combinations

score = zeros(nspec,nspec);

% get dimensions input raster
[r,c] = size(A);

% increase A by one to avoid zero-indexing
A = A+1;


% count neighbour types
for i = 1:r
    for j = 1:c
        % horizontal neighbours (current column & column to the right)
        score(A(i,j),A(mod(i,r)+1,j)) = score(A(i,j),A(mod(i,r)+1,j)) + 1;
        
        % vertical neighbours (current row and row below)
       score(A(i,j),A(i,mod(j,c)+1)) = score(A(i,j),A(i,mod(j,c)+1)) + 1;
       
    end
end

% calculate proportions from score matrix
for i = 1:nspec
    for j = 1:nspec
        score(i,j)=score(i,j)/(r*c*2);
    end
end

end


