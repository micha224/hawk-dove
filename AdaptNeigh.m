function [NE] = AdaptNeigh(E, Payment)
%ADAPTNEIGH Summary of this function goes here
%   Detailed explanation goes here

    % get field size
    N = size(E,1);
    
    % evaluate payment status of neighbours
    for i=1:N
        for j=1:N
            pay = Payment(i,j);
            NE(i,j) = E(i,j);
            for k=-1:1
                for h=-1:1
                    % Taking account of boundary conditions
                    a = cdc(i+k,N); b = cdc(j+h,N);
                    % If the neighbour performed better, the i,j player
                    % changes and mimics the neighbour
                    if (Payment(a,b) > pay)
                        pay = Payment(a,b);
                        NE(i,j) = E(a,b);
                    end
                end
            end
        end
    end

    NE = NE;
end

