function [y,z] = CountNeigh(x,A)
%   Count the number of neighbours in state x
%   includes torus boundaries 

[m,n] = size(A);

north = [n 1:n-1];
east = [2:m 1];
south = [2:n 1];
west = [m 1:m-1];

I = find(A == x);
A = zeros(n,m);
A(I) = 1;  

  y = A(north,:) + A(south,:) + A(:,east) + ...
      A(:,west) + A(north,east) + A(north,west) + ...
       A(south,east) + A(south,west);
  
end

