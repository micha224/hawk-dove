%% ------------------------------------------------------------
%  SPATIAL ALTRUISM MODEL

% Adapted by Maarten Boerlijst & Runa Magnusson
%  <boerlijst@uva.nl>

% The model simulates spread of an altruistic gene that decreases the
% probability of dying of neighbour individuals, but increases the
% deathrate of the altruistic individuals

%% 1.1 INITIALISATION - MODEL ESSENTIALS
% Do not change

clc; clear all; close all; rng(44);

% build colormap
map = [ 0 0 0 
        0 1 1 
        1 0 0 
        1 1 0];

%% 1.2 INITIALISATION - CONTROL CONSTANTS
% Set size, time span, delay and input/output of the model
    
% delay factor in seconds (delay=0 means no delay)  
delay = 0.01;  
    
% set interval between drawing states (speeds up run time)
drawstep=100;

% set name of matrix to load or save
% startfile = 'MutStart.mat';

% Set Parameters
max_iter = 21474836;
current_iter = 0;

% Automaton size
N = 100;

%% 1.3 INITIALISATION - SYSTEM CONSTANTS
% Set system parameters that will control species distribution and
% behaviour

% basic growth rate
p_growth = 0.05;

% deathrate probability  
p_death = 0.03;

% costs -> growth rate reduction (cannot exceed p_growth)
Fight_cost = 0.6;
Bluff_cost = 0.04;

% Reward -> growth rate increase per altruist neighbour
Reward = 0.2;

% Movement probability
p_move = 0.00;

%% 1.4 INITIALISATION - ALLOCATIONS
% Do not change

% Allocations of system variables
pA = zeros(max_iter+1,1);
pM = zeros(max_iter+1,1);
pU = zeros(max_iter+1,1);
pE = zeros(max_iter+1,1);
clusterAA = zeros(max_iter+1,1);
clusterEE = zeros(max_iter+1,1);
clusterAE = zeros(max_iter+1,1);
E = zeros(N,N);
NE = zeros(N,N);
Pgrowth= zeros(N,N);

% determine maximum growth rate  
maxgrowth =p_growth+8*Reward;


%% 2.1 MODEL - INITIAL CONDITIONS

% load an existing or a custom start situation
% 0 = unoccupied
% 1 = Dove
% 2 = Hawk
% 3 = Prober

A = rand(N,N);
I = find(A < 0.05);
E(I) = 1;
I = find(A < 0.025);
E(I) = 3;
I = find(A < 0.003);
E(I) = 2;

%change this if you want another start configuration
%startmap = csvread('four blocks.csv');
%E = startmap;

% introduce the mutant in the right half of the field
%{
for i=1:N
    for j=N/2:N
       if (E(i,j) == 1)
          E(i,j)=3;
       end
    end
end
%}
%draw initial state and pause
figure(1);
set(gcf,'Position',[230 130 400 380])
image(E+1);  
colormap(map);
title('Initial Pattern -> hit key to continue')
pause;

ButtonH = uicontrol('Style', 'ToggleButton', 'Position',[5 5 30 30], ...
    'String', 'HALT', 'Value', 0);
ButtonStop = uicontrol('Style', 'ToggleButton', 'Position',[40 5 30 30], ...
    'String', 'STOP', 'Value', 0);

f2=figure(2); clf
set(gcf,'Position',[700 130 450 380])





%% 2.2 MODEL - GLOBAL LOOP
for current_iter=0:max_iter
    
    % default newstate = oldstate
    NE=E;
    
    % counting the number of altruists in the 8 cell neighborhood
    Ncount1=CountNeigh(1,E);
    Ncount2=CountNeigh(2,E);
    Ncount3=CountNeigh(3,E);
    
    % calculate growthrates  
    for i=1:N
        for j=1:N
            switch E(i,j)   %look at cost/rewards for bluff
                case 0 
                    Pgrowth(i,j)=0;
                case 1
                    Pgrowth(i,j)=p_growth+ (Reward/2)*Ncount1(i,j)+ 0*Ncount2(i,j) + 0*Ncount3(i,j);
                case 2
                    Pgrowth(i,j)=p_growth + Reward*Ncount1(i,j) + (Reward)*Ncount3(i,j) + (Reward-Fight_cost)/2*Ncount2(i,j);
                case 3 
                    Pgrowth(i,j)=p_growth + (Reward - Bluff_cost)* Ncount1(i,j)- Bluff_cost*Ncount2(i,j) + ((Reward - Bluff_cost)/2)*Ncount3(i,j);
           end
        end
    end                
                    
    % Calculating Next State
    for i=1:N
        for j=1:N
            switch E(i,j)  
                case 0     % if cell is empty -> random growth with probability p_growth
                   neigh=randi(8); % pick a random neighbour
                    if (rand < SelectNeigh(Pgrowth,neigh,i,j))   % check the growth rate
                        NE(i,j)= SelectNeigh(E,neigh,i,j);  %create an offspring
                     end
                otherwise   
                     if (rand < p_death)   % density-independent deathrate
                         NE(i,j)=0;
                     end
            end
         end
    end

    
    
    
    %% 2.3 MODEL - VISUALISATION
    
    if mod(current_iter,drawstep)==0
    
     % drawing state 
       figure(1)
       imagesc(E); 
       colormap(map)
       caxis([0 3])
       title(['Generation, ',num2str(current_iter)])
       drawnow
       text_1 = sprintf('Cf_%d_Cb_%d_I_%d.png',Fight_cost, Bluff_cost, current_iter);
       print(figure(1),text_1,'-dpng');

    % drawing payments 

        figure(2)
        imagesc(Pgrowth);
        colormap('Bone');
        colorbar;
        caxis([0 maxgrowth])
        title('growthrate')
        drawnow
    
   
  
    % if delay was set to fixed amount of seconds, wait that amount
  
        pause(delay)
    end
    
    %% 2.4 MODEL- EVALUATE USER CONTROL PANEL
    % If user has pressed stop button, break to implement a change in the 
    % system 

   if get(ButtonH, 'Value') == 1
    title(['Generation, ',num2str(current_iter),'    hit key to continue'])
    pause;
    figure(1)
    ButtonH = uicontrol('Style', 'ToggleButton', 'Position',[5 5 60 30], ...
    'String', 'HALT', 'Value', 0);
   end
   
   %Stops the iteration process
   if get(ButtonStop, 'Value') == 1
    break;
   end
 
   
    %% 2.5 MODEL - STATS + UPDATE
    
    % proportion of cooperators, egoists and dead cells
    pA(current_iter+1) = length(find(E==1)) / (N*N);
    pM(current_iter+1) = length(find(E==3)) / (N*N);
    pE(current_iter+1) = length(find(E==2)) / (N*N);
    pU(current_iter+1) = length(find(E==0)) / (N*N);
    
    % clustering of coorperators and defectors
    % theoretical clustering based on proportions
    names = {'dead-dead';'dead-alt';'dead-ego';'ego-ego'; 'ego-alt'; 'alt-alt'};
    expe = [pU(current_iter+1)^2; 2*pU(current_iter+1)*pA(current_iter+1); 2*pU(current_iter+1)*pE(current_iter+1); ...
    pE(current_iter+1)^2; 2*pE(current_iter+1)*pA(current_iter+1); pA(current_iter+1)^2];

    % actual clustering (counted in clustercalc function)
    obs = clustercalc(NE,4);
    if (pA(current_iter+1)> 0) 
        clusterAA(current_iter+1) = obs(2,2)/expe(6);
    end
    if (pE(current_iter+1)>0)
        clusterEE(current_iter+1) = obs(3,3)/expe(4);
    end
    if (pA(current_iter+1) > 0 && pE(current_iter+1) > 0)
        clusterAE(current_iter+1) = (obs(2,3)+obs(3,2))/expe(5);
    end
    
    % updating the next state to the current state
    NE = MeanField(NE);
    E = NE;

end %iter

%% 3.1 OUTPUT - FINISH .AVI MOVIES AND PLOT STATISTICS

cpA = pA(1:end-((max_iter)-current_iter),1);
cpM = pM(1:end-((max_iter)-current_iter),1);
cpE = pE(1:end-((max_iter)-current_iter),1);
cpU = pU(1:end-((max_iter)-current_iter),1);
text = fprintf('pDove: %d, cpH: %d, cpP: %d',cpA(current_iter),cpE(current_iter),cpM(current_iter));
disp(text);
% timeplot PROPORTION of cooperators
figure(4); %ik heb iets veranderd
set(gcf,'Position',[160 660 440 340])
plot(0:current_iter,cpA,'-co');
hold on;
plot(0:current_iter,cpM,'-yo');
hold on;
plot(0:current_iter,cpE,'-ro');
hold on;
plot(0:current_iter,cpU,'-ko');
xlabel('Generation')
ylabel('Proportion')
legend({'Dove', 'Bully', 'Hawk', 'unoccupied'})
%grid on;

% timeplot CLUSTERING of cooperators
% figure(5)
% set(gcf,'Position',[610 660 440 340])
% plot(0:max_iter,clusterAA,'-gs')
% hold on
% plot(0:max_iter,clusterEE,'-rs')
% xlabel('Iteration')
% ylabel('Clustering Index')
% legend({'alt-alt', 'ego-ego'})
% grid on;
% 
% figure(6)
% set(gcf,'Position',[800 660 440 340])
% plot(0:max_iter,clusterAE,'-bs')
% xlabel('Iteration')
% ylabel('Clustering Index')
% legend({'alt-ego'})
% grid on;

% if desired: save state created
save('EndState.mat','E')